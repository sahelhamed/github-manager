import React from 'react';
import { syncHistoryWithStore } from 'react-router-redux';
import { Route, Router, Switch } from 'react-router-dom';
import { Provider } from 'react-intl-redux';
import { makeStyles } from '@material-ui/core/styles';
import { hashHistory } from './utils/history';
import { store } from './store/store';
import Header from './components/Header';
import './assets/style.css';
import Home from './routes/Home';
import Organizations from './routes/Organizations';
import Repositories from './routes/Repositories/Repositories';


const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(8)
  }
}));


const App = () => {
  const classes = useStyles();

  return (
    <Provider store={store}>
      <Router history={syncHistoryWithStore(hashHistory, store)}>
        <Header />
        <Route path="/" exact>
          <Home />
        </Route>
        <div className={classes.root}>
          <Route path="/organizations" exact>
            <Organizations />
          </Route>
          <Route path="/repositories" exact>
            <Repositories />
          </Route>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
