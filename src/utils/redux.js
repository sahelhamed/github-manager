
const createConstants = (...constants) => {
  return constants.reduce((acc, constant) => {
    Object.assign(acc, { [constant]: constant });
    return acc;
  }, {});
};

const createReducer = (initialState, reducersMap) => {
  return (state = initialState, action) => {
    const reducer = reducersMap[action.type];
    return reducer
      ? ({ ...state, ...reducer(state, action.payload, action.meta) })
      : state;
  };
};


export { createConstants, createReducer };
