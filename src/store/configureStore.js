
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import reducers from './reducers';
import sagas from './sagas';


const configureStore = (initialState = {}) => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];
  const enhancers = [composeWithDevTools(applyMiddleware(...middlewares))];
  const store = createStore(reducers, initialState, compose(...enhancers));

  sagaMiddleware.run(sagas);

  return store;
};

export default configureStore;
