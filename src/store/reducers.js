
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { intlReducer } from 'react-intl-redux';
import userReducer from './user/reducer';


const mainReducer = combineReducers({
  routing: routerReducer,
  intl: intlReducer,
  user: userReducer
});

const rootReducer = (state, action) => mainReducer(state, action);

export default rootReducer;
