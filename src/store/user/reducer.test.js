/* eslint-disable */

import reducer from './reducer';
import * as actionsType from './constants';

describe('user reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      user: null,
      organizations: [],
      repositories: [],
      isLoading: false,
      error: null
    });
  });
  it('should store the repositories', () => {
    expect(reducer({
      user: null,
      organizations: [],
      repositories: [],
      isLoading: false,
      error: null
    }, { username: 'some-username', type: actionsType.GET_REPOSITORIES_LIST_SUCCESS })).toEqual({
      user: null,
      organizations: [],
      repositories: [],
      isLoading: false,
      error: null
    });
  });
});
