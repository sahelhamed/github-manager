import ACTIONS from './constants';

export const getRepositoriesListRequest = (username) => ({
  type: ACTIONS.GET_REPOSITORIES_LIST_REQUEST,
  payload: { username }
});

export const getRepositoriesListSuccess = (data) => ({
  type: ACTIONS.GET_REPOSITORIES_LIST_SUCCESS,
  payload: data
});

export const getUserDataRequest = (username) => ({
  type: ACTIONS.GET_USER_DATA_REQUEST,
  payload: { username }
});

export const getUserDataSuccess = (data) => ({
  type: ACTIONS.GET_USER_DATA_SUCCESS,
  payload: data
});

export const fetchFailed = (error) => ({
  type: ACTIONS.FETCH_FAILED,
  payload: error
});
