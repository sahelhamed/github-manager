import { createConstants } from '../../utils/redux';

const constants = createConstants(
  'GET_REPOSITORIES_LIST_REQUEST',
  'GET_REPOSITORIES_LIST_SUCCESS',
  'GET_USER_DATA_REQUEST',
  'GET_USER_DATA_SUCCESS',
  'FETCH_FAILED'
);

export default constants;
