import { put, call, takeEvery } from 'redux-saga/effects';
import { getRepos } from '../../../services';
import { getRepositoriesListSuccess, fetchFailed } from '../actions';
import CONSTANTS from '../constants';


export function* fetchList(action) {
  try {
    const { username } = action.payload;
    const list = yield call(getRepos, username);
    yield put(getRepositoriesListSuccess(list));
  } catch (error) {
    yield put(fetchFailed(error.response.statusText));
  }
}


export function* watchGetRepositoriesList() {
  yield takeEvery(CONSTANTS.GET_REPOSITORIES_LIST_REQUEST, fetchList);
}
