import { fork } from 'redux-saga/effects';
import { watchGetRepositoriesList } from './watchGetRepositoriesList';
import { watchGetUserData } from './watchGetUserData';


export function* user() {
  yield fork(watchGetRepositoriesList);
  yield fork(watchGetUserData);
}
