import { put, call, takeEvery } from 'redux-saga/effects';
import { getUserData } from '../../../services';
import { getUserDataSuccess, fetchFailed } from '../actions';
import CONSTANTS from '../constants';


export function* fetchList(action) {
  try {
    const { username } = action.payload;
    const list = yield call(getUserData, username);
    yield put(getUserDataSuccess(list));
  } catch (error) {
    yield put(fetchFailed(error.response.statusText));
  }
}


export function* watchGetUserData() {
  yield takeEvery(CONSTANTS.GET_USER_DATA_REQUEST, fetchList);
}
