import { createReducer } from '../../utils/redux';
import ACTIONS from './constants';

const initialState = {
  user: null,
  organizations: [],
  repositories: [],
  isLoading: false,
  error: null
};

const getRepositoriesListRequest = (state) => ({
  ...state,
  isLoading: true,
  error: null
});

const getRepositoriesListSuccess = (state, list) => ({
  ...state,
  repositories: list,
  isLoading: false
});

const getUserDataRequest = (state) => ({
  ...state,
  isLoading: true,
  error: null
});

const getUserDataSuccess = (state, list) => ({
  ...state,
  user: list.user,
  organizations: list.orgs,
  isLoading: false
});

const fetchFailed = (state, error) => ({
  ...state,
  error,
  setting: {
    ...state.setting,
    isLoading: false
  },
  isLoading: false
});

export default createReducer(initialState, {
  [ACTIONS.GET_REPOSITORIES_LIST_REQUEST]: getRepositoriesListRequest,
  [ACTIONS.GET_REPOSITORIES_LIST_SUCCESS]: getRepositoriesListSuccess,
  [ACTIONS.GET_USER_DATA_REQUEST]: getUserDataRequest,
  [ACTIONS.GET_USER_DATA_SUCCESS]: getUserDataSuccess,
  [ACTIONS.FETCH_FAILED]: fetchFailed
});
