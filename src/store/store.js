
import configureStore from './configureStore';
import messageEn from '../i18n/en';

const initialState = {
  intl: {
    defaultLocale: 'en',
    locale: 'en',
    messages: messageEn
  }
};

export const store = configureStore(initialState);
