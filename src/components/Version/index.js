import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  version: {
    position: 'absolute',
    left: 20,
    bottom: 20,
    opacity: 0.7,
    fontSize: 14,
    color: '#000000'
  }
}));

const Version = (props) => {
  const { intl: { formatMessage } } = props;
  const classes = useStyles();

  return (
    <div
      className={classes.version}
    >
      {`${formatMessage({ id: 'version' })} 1.0.0`}
    </div>
  );
};

Version.propTypes = {
  intl: PropTypes.object.isRequired
};

Version.defaultProps = {};


export default injectIntl(Version);
