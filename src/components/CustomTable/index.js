import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: theme.spacing(3)
  },
  container: {
    maxHeight: 365
  },
  header: {
    backgroundColor: '#fafafa'
  },
  headerCells: {
    fontSize: 14,
    color: '#546e7a'
  },
  cells: {
    paddingTop: 10,
    paddingBottom: 10,
    color: '#37474f',
    fontSize: 14
  },
  label: {
    textAlign: 'center',
    color: '#ffffff',
    padding: '1px 8px',
    margin: '0 0 2px 2px',
    borderRadius: 4,
    backgroundColor: '#546e7a',
    display: 'inline-block'
  },
  link: {
    color: '#1e88e5',
    textDecoration: 'none'
  }
}));


const CustomTable = (props) => {
  const classes = useStyles();
  const {
    intl: { formatMessage, formatDate }, data, columns
  } = props;

  return (data.length > 0
    && (
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader>
            <TableHead className={classes.header}>
              <TableRow>
                <TableCell className={classes.headerCells}>{formatMessage({ id: 'row' })}</TableCell>
                {columns.map((column) => (
                  <TableCell key={column.id} className={classes.headerCells}>{column.headerTitle}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row, index) => (
                <TableRow key={row.id} hover className="row">
                  <TableCell component="th" scope="row" className={classes.cells}>
                    {index + 1}
                  </TableCell>
                  {columns.map((column) => (
                    <TableCell key={column.id} className={classes.cells} component="th" scope="row">

                      {column.type === 'text' && row[column.property]}

                      {column.type === 'link'
                    && (
                      <a href={column.href} className={classes.link}>
                        {row[column.property]}
                      </a>
                    )}

                      {column.type === 'date'
                        && (
                          <div>
                            {row[column.property] ? formatDate(row[column.property]) : '--' }
                          </div>
                        )}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    )
  );
};

CustomTable.propTypes = {
  intl: PropTypes.object,
  data: PropTypes.array,
  columns: PropTypes.array
};


CustomTable.defaultProps = {
  intl: {},
  data: [],
  columns: []
};

export default injectIntl(CustomTable);
