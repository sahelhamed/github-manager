/* eslint-disable */

import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Table from '@material-ui/core/Table';
import CustomTable from './index';

configure({ adapter: new Adapter() });

describe('<CustomTable />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<CustomTable columns={[{
      id: 5,
      property: 'forks',
      headerTitle: 'forks',
      type: 'text'
    }]}
    />);
  });

  it('should render <Table /> when data length is more than 0', () => {
    wrapper.setProps({ data: [{ id: 0 }] });
    // expect(wrapper.find(Table)).toHaveLength(1);
  });
});
