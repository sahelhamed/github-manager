import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    top: 0,
    right: 0,
    left: 0,
    zIndex: 10
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  margin: {
    marginLeft: theme.spacing(2)
  }
}));

const Header = (props) => {
  const { intl: { formatMessage } } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            {formatMessage({ id: 'github.manager' })}
          </Typography>
          <Link to="/repositories" color="inherit">
            {formatMessage({ id: 'repositories' })}
          </Link>
          <Link to="/organizations" color="inherit" className={classes.margin}>
            {formatMessage({ id: 'organizations' })}
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
};

Header.propTypes = {
  intl: PropTypes.object.isRequired
};

Header.defaultProps = {};

export default injectIntl(Header);
