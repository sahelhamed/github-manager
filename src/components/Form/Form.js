import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import TextField from '@material-ui/core/TextField/index';
import Button from '@material-ui/core/Button/index';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress/index';
import { makeStyles } from '@material-ui/core/styles/index';
import Paper from '@material-ui/core/Paper/index';
import * as repositoryActions from '../../store/user/actions';


const validationSchema = Yup.object().shape({
  // username: Yup.string().required('Required')
});

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: theme.spacing(2)
  },
  button: {
    height: 56,
    marginLeft: theme.spacing(2)
  },
  progress: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  }
}));

const GetUsernameForm = (props) => {
  const {
    intl: { formatMessage }, isLoading, serverError, handleSubmit, title
  } = props;
  const classes = useStyles();

  return (
    <Formik
      initialValues={{ username: '' }}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {({
        errors, touched, values, handleChange, handleBlur
      }) => {
        const errorText = serverError || errors.username;
        return (
          <Form>
            <Paper className={classes.paper}>
              {isLoading && <LinearProgress className={classes.progress} color="primary" />}
              <Typography variant="h6">
                {title}
              </Typography>
              <div>
                <TextField
                  name="username"
                  label={formatMessage({ id: 'username' })}
                  variant="outlined"
                  value={values.username}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errorText || ''}
                  error={touched.username && Boolean(errors.username || serverError)}
                  inputProps={{ "data-testid": "Input" }}
                />
                <Button
                  className={classes.button}
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={!values.username || isLoading}
                  data-testid="submit"
                >
                  {formatMessage({ id: 'submit' })}
                </Button>
              </div>
            </Paper>
          </Form>
        );
      }}
    </Formik>
  );
};


const mapStateToProps = (state) => {
  return {
    repositories: state.user.repositories,
    isLoading: state.user.isLoading,
    serverError: state.user.error
  };
};

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(repositoryActions, dispatch)
});

GetUsernameForm.propTypes = {
  intl: PropTypes.object.isRequired,
  title: PropTypes.string,
  handleSubmit: PropTypes.func,
  isLoading: PropTypes.bool,
  serverError: PropTypes.string
};

GetUsernameForm.defaultProps = {
  title: '',
  isLoading: false,
  serverError: null,
  handleSubmit: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(GetUsernameForm));
