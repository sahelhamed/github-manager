/* eslint-disable */

import * as React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { Formik, Form, Field } from 'formik';
import {
  render, fireEvent, waitForElement, wait
} from '@testing-library/react';

describe('get username form', () => {
  it('submits values and fires', () => {
    const mock = jest.fn();
    const { getByText, getByTestId } = render(
      <Formik initialValues={{ username: '' }} onSubmit={mock}>
        <Form>
          <Field name="username" data-testid="Input" />
          <button type="submit" data-testid="submit">Submit</button>
        </Form>
      </Formik>
    );
    /*
       const input = waitForElement(() => getByTestId('Input'));
       const button = waitForElement(() => getByTestId('submit'));

       fireEvent.change(input, {
         target: {
           value: 'Charles'
         }
       });

       fireEvent.click(button);

       wait(() => {
         expect(mock).toBeCalled();
         expect(mock.mock.calls[0][0].name).toBe('Charles');
       }); */
  });
});
