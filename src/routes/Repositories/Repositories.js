
import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GetUsernameForm from '../../components/Form/Form';
import * as repositoryActions from '../../store/user/actions';
import CustomTable from '../../components/CustomTable';


const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Repositories = (props) => {
  const { intl: { formatMessage }, actions, repositories } = props;
  const classes = useStyles();

  const handleSubmit = (values) => {
    actions.getRepositoriesListRequest(values.username);
  };

  const columns = [
    {
      id: 0,
      property: 'name',
      headerTitle: formatMessage({ id: 'name' }),
      type: 'text'
    },
    {
      id: 1,
      property: 'ssh_url',
      headerTitle: formatMessage({ id: 'ssh.url' }),
      type: 'text'
    },
    {
      id: 2,
      property: 'clone_url',
      headerTitle: formatMessage({ id: 'clone.url' }),
      type: 'text'
    },
    {
      id: 3,
      property: 'watchers',
      headerTitle: formatMessage({ id: 'watchers' }),
      type: 'text'
    },
    {
      id: 4,
      property: 'size',
      headerTitle: formatMessage({ id: 'size' }),
      type: 'text'
    },
    {
      id: 5,
      property: 'forks',
      headerTitle: formatMessage({ id: 'forks' }),
      type: 'text'
    }
  ];

  return (
    <div className={classes.root}>
      <GetUsernameForm title={formatMessage({ id: 'repositories' })} handleSubmit={handleSubmit} />
      <CustomTable data={repositories} columns={columns} />
    </div>
  );
};


const mapStateToProps = (state) => {
  return {
    repositories: state.user.repositories,
    isLoading: state.user.isLoading
  };
};

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(repositoryActions, dispatch)
});

Repositories.propTypes = {
  intl: PropTypes.object.isRequired,
  repositories: PropTypes.array,
  actions: PropTypes.object.isRequired
};

Repositories.defaultProps = {
  repositories: []
};


export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Repositories));
