
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as repositoryActions from '../../store/user/actions';
import GetUsernameForm from '../../components/Form/Form';
import CustomTable from '../../components/CustomTable';


const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Organizations = (props) => {
  const { intl: { formatMessage }, actions, organizations } = props;
  const classes = useStyles();

  const handleSubmit = (values) => {
    actions.getUserDataRequest(values.username);
  };


  const columns = [
    {
      id: 0,
      property: 'avatar_url',
      headerTitle: formatMessage({ id: 'avatar.url' }),
      type: 'link'
    },
    {
      id: 1,
      property: 'url',
      headerTitle: formatMessage({ id: 'url' }),
      type: 'link'
    },
    {
      id: 2,
      property: 'repos_url',
      headerTitle: formatMessage({ id: 'repository.url' }),
      type: 'link'
    },
    {
      id: 3,
      property: 'description',
      headerTitle: formatMessage({ id: 'description' }),
      type: 'text'
    }
  ];

  return (
    <div className={classes.root}>
      <GetUsernameForm title={formatMessage({ id: 'organizations' })} handleSubmit={handleSubmit} />
      <CustomTable data={organizations} columns={columns} />
    </div>
  );
};


const mapStateToProps = (state) => {
  return {
    organizations: state.user.organizations,
    isLoading: state.user.isLoading
  };
};

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(repositoryActions, dispatch)
});

Organizations.propTypes = {
  intl: PropTypes.object.isRequired,
  organizations: PropTypes.array,
  actions: PropTypes.object.isRequired
};

Organizations.defaultProps = {
  organizations: []
};


export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Organizations));
