
import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { hashHistory } from '../../utils/history';
import Version from '../../components/Version';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  paper: {
    padding: theme.spacing(8),
    textAlign: 'center'
  },
  button: {
    marginTop: theme.spacing(4)
  }
}));

const Home = (props) => {
  const {
    intl: { formatMessage }
  } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Typography variant="h6" className={classes.title}>
          {formatMessage({ id: 'welcome.to.github.manager' })}
        </Typography>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => hashHistory.push('/repositories')}
        >
          {formatMessage({ id: 'get.started' })}
        </Button>
      </Paper>
      <Version />
    </div>
  );
};


Home.propTypes = {
  intl: PropTypes.object.isRequired
};

Home.defaultProps = {};


export default injectIntl(Home);
