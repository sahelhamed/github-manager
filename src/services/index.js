import axios from 'axios';

// Documentation is at https://developer.github.com/v3/
const BASE_URL = 'https://api.github.com';

const getRepos = async (username) => {
  const url = `${BASE_URL}/users/${username}/repos?per_page=250`;
  const serverResponse = await axios.get(url).then((response) => {
    return response.data;
  }).catch((error) => {
    throw error;
  });

  return serverResponse;
};

const getUserData = async (username) => {
  return axios
    .all([
      await axios.get(`${BASE_URL}/users/${username}`),
      await axios.get(`${BASE_URL}/users/${username}/orgs`)
    ])
    .then(([user, orgs]) => ({
      user: user.data,
      orgs: orgs.data
    })).catch((error) => {
      throw error;
    });
};

export { getRepos, getUserData };
