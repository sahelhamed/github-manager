
export default {
  'welcome.to.github.manager': 'Welcome to GitHub Manager',
  'get.started': 'Get started',
  'github.manager': 'GitHub Manager',
  'version': 'Version',
  'submit': 'Submit',
  'username': 'Username',
  'repositories': 'Repositories',
  'organizations': 'Organizations',
  'row': 'Row',
  'name': 'Name',
  'description': 'Description',
  'watchers': 'Watchers',
  'forks': 'Forks',
  'ssh.url': 'SSH URL',
  'repository.url': 'Repository URL',
  'url': 'URL',
  'size': 'Size',
  'clone.url': 'Clone URL',
  'avatar.url': 'Avatar URL'
};
